REGISTRY=local
IMAGE_NAME=$(REGISTRY)/yoco_jack
VERSION=0.1.0

default: help

help:
	@echo "yoco_jack: An implementation of the YocoJack game."
	@echo ""
	@echo "Usage:"
	@echo "    make build-docker        build the docker image that includes the tool"
	@echo "    make run                 run all jobs"
	@echo "    make test                run all tests"
	@echo ""
	@echo "Authors:"
	@echo "    Shannon Black <shannon@netforge.co.za>"


build-docker:
	docker build --rm -t $(IMAGE_NAME):$(VERSION) ./

run:
	docker run --net="host" -p 8900:8888 -it $(IMAGE_NAME):$(VERSION) yoco_jack run_game

test:
	docker run -it $(IMAGE_NAME):$(VERSION) python setup.py test

run-bash:
	@echo running docker under bash [TESTING PURPOSES ONLY]
	docker run -it $(IMAGE_NAME):$(VERSION) bash