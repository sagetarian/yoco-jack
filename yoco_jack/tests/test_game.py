import pytest
import json

import yoco_jack

def test_load_url():
    hands = yoco_jack.game.load_from_url('https://s3-eu-west-1.amazonaws.com/yoco-testing/tests.json')
    assert len(hands) == 108

def test_game_logic():
    with open('./yoco_jack/tests/tests.json') as f:
        content = f.read()
        games = json.loads(content)

        for hands in games:
            is_player_A_winner = yoco_jack.game.play(hands)
            assert is_player_A_winner == hands['playerAWins']
