import requests
from functools import reduce

from yoco_jack.card import hand_details


class GameException(Exception):
    pass

def load_from_url(url):
    """
    return a list object of games in the format of:
     {
        "playerA" : (list)
        "playerB" : (list)
        "playerAWins": (boolean)
     }
     :returns a list of games
    """
    try:
        response = requests.get(url)
        if response.status_code == 200:
            response_obj = response.json()
            if isinstance(response_obj, list):
                return response_obj
            raise GameException("Invalid game format from url {url}".format(
                url=url
            ))
        else:
            raise GameException("Status code {status_code} from url {url}".format(
                status_code=response.status_code,
                url=url
            ))
    except:
        raise GameException("Unknown issue downloading games from url {url}".format(
            url=url
        ))

def play(game):
    """
    simulates a single game.
     :returns whether playerA wins (boolean)
    """
    if 'playerA' not in game or 'playerB' not in game:
        raise GameException("Invalid game format")

    return is_first_hand_winner(game['playerA'], game['playerB'])

def is_first_hand_winner(first_hand, second_hand):
    """
    given two hands in a game, determine if the first hand wins
    """

    # evaluate the hands
    first_hand = hand_details(first_hand)
    second_hand = hand_details(second_hand)

    # first test against score
    first_hand_score = reduce(__reduce_score_func, first_hand)
    second_hand_score = reduce(__reduce_score_func, second_hand)

    # determine winner by score if we can
    winner_by_score = __is_first_hand_winner_by_score(first_hand_score, second_hand_score)
    if winner_by_score is not None:
        return winner_by_score

    # following rankings must be done by card value rank
    sorted_first_hand = sorted(first_hand, reverse=True, key=lambda x:x['rank'])
    sorted_second_hand = sorted(second_hand, reverse=True, key=lambda x:x['rank'])


    # determine winner by card rank
    first_hand_ranks = list(map(lambda x: x['rank'], sorted_first_hand))
    second_hand_ranks = list(map(lambda x: x['rank'], sorted_second_hand))

    winner_by_rank = __is_first_hand_winner_by_rank(first_hand_ranks, second_hand_ranks)
    if winner_by_rank is not None:
        return winner_by_rank

    # only get the top ranked card to test sort suite ranks
    first_hand_top_rank_only = list(filter(lambda x: x['rank'] == sorted_first_hand[0]['rank'], sorted_first_hand))
    second_hand_top_rank_only = list(filter(lambda x: x['rank'] == sorted_second_hand[0]['rank'], sorted_second_hand))

    sorted_first_hand = sorted(first_hand_top_rank_only, reverse=True, key=lambda x: x['suite_rank'])
    sorted_second_hand = sorted(second_hand_top_rank_only, reverse=True, key=lambda x: x['suite_rank'])

    return sorted_first_hand[0]['suite_rank'] > sorted_second_hand[0]['suite_rank']

def __is_first_hand_winner_by_score(first_hand_score, second_hand_score):
    # instantly lose (BUSTED!!)
    if first_hand_score > 21:
        return False

    # instantly win (second hand BUSTED!!)
    if second_hand_score > 21:
        return True

    # cannot determine by score
    if first_hand_score == second_hand_score:
        return None

    # either first hand wins or second
    return first_hand_score > second_hand_score

def __is_first_hand_winner_by_rank(first_hand_ranks, second_hand_ranks):
    # first_hand_sorted_rank = sorted(first_hand_ranks, reverse=True)
    # second_hand_sorted_rank = sorted(second_hand_ranks, reverse=True)

    i = 0
    while i < len(first_hand_ranks):
        if first_hand_ranks[i] == second_hand_ranks[i]:
            i = i + 1
            continue
        return first_hand_ranks[i] > second_hand_ranks[i]
    return None

def __reduce_score_func(x, y):
    if not isinstance(x, int):
        return x['value'] + y['value']
    return x + y['value']