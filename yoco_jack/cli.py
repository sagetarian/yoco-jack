import click
import requests

import yoco_jack

@click.group()
def cli(chain=True):
    pass

@cli.command()
def run_game():
    games = yoco_jack.game.load_from_url('https://s3-eu-west-1.amazonaws.com/yoco-testing/tests.json')
    for hands in games:

        player_A_hand = hands['playerA']
        player_B_hand = hands['playerB']

        is_player_A_winner = yoco_jack.game.is_first_hand_winner(player_A_hand, player_B_hand)

        # shorthand for the above
        # is_player_A_winner = yoco_jack.game.play(hands)


        print ("player {winner} won and {match} the expected winner".format(
            winner="A" if is_player_A_winner else "B",
            match="matches" if is_player_A_winner == hands['playerAWins'] else 'does not match'
            # hands=str(hands))
        ))