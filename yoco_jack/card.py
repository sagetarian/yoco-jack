import yoco_jack

def hand_details(hand):
    scores = []
    for card in hand:
        scores.append(__card_details(card))
    return scores

def __card_details(card):
    valid_card_non_digits = 'JQKA'

    card_value = card[:-1]
    card_suite = card[-1:]
    if card_value.isdigit():
        return {
            'rank': int(card_value),
            'value': int(card_value),
            'suite_rank': __suite_rank(card_suite)
        }

    if card_value not in valid_card_non_digits:
        raise yoco_jack.game.GameException("Invalid card value for card {card}".format(card=card))

    return {
        'rank': __special_card_rank(card_value),
        'suite_rank': __suite_rank(card_suite),
        'value': 11 if card_value == 'A' else 10
    }



def __special_card_rank(card_value):
    """
    determine the rank of a special card relative to the 1-10 cards
    """
    return 11 + 'JQKA'.index(card_value)

def __suite_rank(card_suite):
    """
    determine rank of the suite
    """
    return 'DCHS'.index(card_suite)