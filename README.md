# yoco_jack

An implementation of the YocoJack game.

# Docker Usage

Usage:

    make build-docker        build the docker image that includes the tool
    make run                 run the docker image and the initial run of games
    make test                run all tests

Quick Run:

```shell
make build-docker run
```

# Python Package Usage

### Install

Requires python 3.6. (Python 2 support has not been tested)

```shell
python setup.py install
```

### Run on host as application

```shell
$yoco_jack run_game
player A won and matches the expected winner
player A won and matches the expected winner
player B won and matches the expected winner
```

### Run tests

```shell
python setup.py tests
```

### Import the yoco_jack package

```python
import yoco_jack
```

### Load yoco_jack format file from a url

```python
hands = yoco_jack.game.load_from_url('https://s3-eu-west-1.amazonaws.com/yoco-testing/tests.json')
```

### Run a game and determine if the first hand wins

Parameters:

- hands is a json object in the format of:

```json
{
    "playerA": [
        "9S",
        "2S"
    ],
    "playerB": [
        "5D",
        "7C"
    ]
}
```

Usage:

```python
is_player_A_winner = yoco_jack.game.play(hands)
```

### Given two hands apply game logic

Parameters:

- Player A Hand
- Player B Hand

Hand format:

```json
    [
        "9S",
        "2S"
    ]
```

Usage:

```python
is_player_A_winner = yoco_jack.game.is_first_hand_winner(player_A_hand, player_B_hand)
```

# Authors

In order to build a list of people with domain knowledge:

* Shannon Black <shannon@netforge.co.za>