FROM python:3.6-jessie
MAINTAINER Shannon Black <shannon@ilovezoona.com>

RUN easy_install click requests

COPY ./ /var/yoco_jack
WORKDIR /var/yoco_jack
RUN python setup.py install

# cleanup
RUN rm -rf /var/lib/apt/lists/*

CMD ['python']