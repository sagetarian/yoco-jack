#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

requirements = [
    'click',
    'requests'
    # enter requirements here
]

setup_args = {
    'name': 'yoco_jack',
    'version': '0.1b',
    'description': 'An implementation of the YocoJack game.',
    'long_description': 'An implementation of the YocoJack game.',
    'author': "Shannon Black",
    'author_email': 'shannon@ilovezoona.com',
    'url': '',
    'packages': find_packages(),
    'package_dir': {
        "yoco_jack": 'yoco_jack'
    },
    'install_requires': requirements,
    'setup_requires': ['pytest-runner'],
    'tests_require': ['pytest', 'pytest-mock'],
    'keywords': 'OCR',
    'classifiers': [
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Yoco',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
    ],
    'entry_points': {
        'console_scripts': [
            'yoco_jack=yoco_jack.cli:cli',
        ],
    },
    'data_files': [
        ('/etc/notifications_api', [])
    ]
}

setup(**setup_args)
